---
title: Organizacijska struktura
slug: organizacijska-struktura
markdown:
    extra: true
---
<div class="structures-container">
    <div class="structures">
        <ul class="structures-tab-links">
            <li id="uprava" class="structures-tab-link active" data-tab="tab-1"><h4>Uprava</h4></li>
            <span class="structures-tab-divider"></span>
            <li id="projektanti" class="structures-tab-link" data-tab="tab-2"><h4>Projektiranje</h4></li>
            <span class="structures-tab-divider"></span>
            <li id="nadzor" class="structures-tab-link" data-tab="tab-3"><h4>Nadzor</h4></li>
            <span class="structures-tab-divider"></span>
            <li id="gradenje" class="structures-tab-link" data-tab="tab-4"><h4>Građenje</h4></li>
        </ul>
        <div id="tab-1" class="structures-tab active">
            <ul class="structure-list">
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>emil.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">direktor</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, dipl.ing.građ.</a></b><br>
                        <a href="mailto:tomislav.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tomislav.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">vlasnik</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="">Snježana Krznarić</a></b><br>
                        <a href="mailto:uprava@infraterra.com.hr"><i class="fa fa-envelope"></i>uprava@infraterra.com.hr</a><br>
                        <span class="upper">voditelj računovodstva i financija</span><br>
                    </div>
                </li>
            </ul>
        </div>
        <div id="tab-2" class="structures-tab">
            <ul class="structure-list">
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>emil.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">ovlašteni inženjer</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, dipl.ing.građ.</a></b><br>
                        <a href="mailto:tomislav.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tomislav.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">ovlašteni inženjer</span><br>
                    </div>
                </li>
                 <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tajana-krznaric-788270143">Tajana Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tajana.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">ovlašteni inženjer</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/ivana-varga-065155b2">Ivana Varga, mag.ing.aedif.</a></b><br>
                        <a href="mailto:uprava@infraterra.com.hr"><i class="fa fa-envelope"></i>ivana.varga@infraterra.com.hr</a><br>
                    </div>
                </li>
                  <li>
                    <div>
                        <b><a href="http://linkedin.com/in/matija-dunatov-02868710b">Matija Dunatov, mag.ing.aedif.</a></b><br>
                        <a href="mailto:uprava@infraterra.com.hr"><i class="fa fa-envelope"></i>matija.dunatov@infraterra.com.hr</a><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="">Leon Krznarić</a></b><br>
                        <a href="mailto:uprava@infraterra.com.hr"><i class="fa fa-envelope"></i>leon.krznaric@infraterra.com.hr</a><br>
                    </div>
                </li>
            </ul>
        </div>
        <div id="tab-3" class="structures-tab">
            <ul class="structure-list">
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>emil.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">ovlašteni inženjer</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, dipl.ing.građ.</a></b><br>
                        <a href="mailto:tomislav.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tomislav.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">ovlašteni inženjer</span><br>
                    </div>
                </li>
                 <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tajana-krznaric-788270143">Tajana Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tajana.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">ovlašteni inženjer</span><br>
                    </div>
                </li>
            </ul>
        </div>
        <div id="tab-4" class="structures-tab">
            <ul class="structure-list">
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, dipl.ing.građ.</a></b><br>
                        <a href="mailto:tomislav.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tomislav.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">ovlašteni voditelj građenja</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>emil.krznaric@infraterra.com.hr</a><br>
                    </div>
                </li>
            </ul>
            <div class="structure-text"><p>Tvrtka <b>INFRATERRA</b> pored osoblja u stalnom radnom odnosu, ima kvalitetnu suradnju i sa nizom stručnjaka iz matičnog područja djelatnosti tvrtke, drugim pravnim subjektima kao i sa državnim tvrtkama i institucijama.</p></div>
        </div>
    </div>
</div>
