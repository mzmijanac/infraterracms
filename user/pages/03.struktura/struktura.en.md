---
title: Organizational structure
slug: organizational-structure
markdown:
    extra: true
---
<div class="structures-container">
    <div class="structures">
        <ul class="structures-tab-links">
            <li id="uprava" class="structures-tab-link active" data-tab="tab-1"><h4>Administration</h4></li>
            <span class="structures-tab-divider"></span>
            <li id="projektanti" class="structures-tab-link" data-tab="tab-2"><h4>Design</h4></li>
            <span class="structures-tab-divider"></span>
            <li id="nadzor" class="structures-tab-link" data-tab="tab-3"><h4>Supervision</h4></li>
            <span class="structures-tab-divider"></span>
            <li id="gradenje" class="structures-tab-link" data-tab="tab-4"><h4>Construction</h4></li>
        </ul>
        <div id="tab-1" class="structures-tab active">
            <ul class="structure-list">
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>emil.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">director</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, dipl.ing.građ.</a></b><br>
                        <a href="mailto:tomislav.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tomislav.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">owner</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="">Snježana Krznarić</a></b><br>
                        <a href="mailto:uprava@infraterra.com.hr"><i class="fa fa-envelope"></i>uprava@infraterra.com.hr</a><br>
                        <span class="upper">head of accounting and finance</span><br>
                    </div>
                </li>
            </ul>
        </div>
        <div id="tab-2" class="structures-tab">
            <ul class="structure-list">
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>emil.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">certified civil engineer </span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, dipl.ing.građ.</a></b><br>
                        <a href="mailto:tomislav.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tomislav.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">certified civil engineer </span><br>
                    </div>
                </li>
                 <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tajana-krznaric-788270143">Tajana Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tajana.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">certified civil engineer </span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/ivana-varga-065155b2">Ivana Varga, mag.ing.aedif.</a></b><br>
                        <a href="mailto:uprava@infraterra.com.hr"><i class="fa fa-envelope"></i>ivana.varga@infraterra.com.hr</a><br>
                    </div>
                </li>
                  <li>
                    <div>
                        <b><a href="http://linkedin.com/in/matija-dunatov-02868710b">Matija Dunatov, mag.ing.aedif.</a></b><br>
                        <a href="mailto:uprava@infraterra.com.hr"><i class="fa fa-envelope"></i>matija.dunatov@infraterra.com.hr</a><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="">Leon Krznarić</a></b><br>
                        <a href="mailto:uprava@infraterra.com.hr"><i class="fa fa-envelope"></i>leon.krznaric@infraterra.com.hr</a><br>
                    </div>
                </li>
            </ul>
        </div>
        <div id="tab-3" class="structures-tab">
            <ul class="structure-list">
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>emil.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">certified contractor's supervisor</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, dipl.ing.građ.</a></b><br>
                        <a href="mailto:tomislav.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tomislav.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">certified contractor's supervisor</span><br>
                    </div>
                </li>
                 <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tajana-krznaric-788270143">Tajana Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tajana.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">certified contractor's supervisor</span><br>
                    </div>
                </li>
            </ul>
        </div>
        <div id="tab-4" class="structures-tab">
            <ul class="structure-list">
               <li>
                    <div>
                        <b><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, dipl.ing.građ.</a></b><br>
                        <a href="mailto:tomislav.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>tomislav.krznaric@infraterra.com.hr</a><br>
                        <span class="upper">certified construction manager</span><br>
                    </div>
                </li>
                <li>
                    <div>
                        <b><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></b><br>
                        <a href="mailto:emil.krznaric@infraterra.com.hr"><i class="fa fa-envelope"></i>emil.krznaric@infraterra.com.hr</a><br>
                    </div>
                </li>
            </ul>
            <div class="structure-text"><p>Besides the permanent staff, the company <b>INFRATERRA</b> has good cooperation with a number of experts from the company's activity area, other legal entities as well as with the state companies and institutions.</p></div>
        </div>
    </div>
</div>


