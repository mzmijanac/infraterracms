---
title: About us
slug: about-us
---

Infraterra d.o.o. was established in 1996 as a privately owned company and registered at the Commercial Court in Zagreb for carrying out design, supervision, consulting and construction activities.

We are a company with a tradition of continuous work for over 20 years and with great intellectual capital, ready for the most complex business operations. The company experts are certified engineers with experience in making project documentation, construction and expert supervision.

Through our knowledge and experience, we cover all phases of a wide range of infrastructure projects such as main gas pipelines, water supply systems, drainage, water supply regulation and wastewater treatment plants.

In our work, we apply modern technology and teamwork, which involves clients, which we consider to be a prerequisite for a successful implementation of all projects. We pay special attention to perfecting the younger staff, which we carry out through continuous education and monitoring of achievements and knowledge.

We are proud of our large number of accomplished projects, as well as those that are just being realized, especially on the experience of drawing up study documentation and project application for the co-financing projects from EU funds.


#####We offer the following services:
* Expert supervision of construction works
* Project Management (IPMA 4-Level-C)
* Occupational safety Coordinator 1 and 2
* Feasibility studies for hydrotechnical systems (water supply, drainage, wastewater treatment)
* Conceptual solutions
* Preliminary, main and construction design
* Construction management and construction of pipelines and WWTPs
