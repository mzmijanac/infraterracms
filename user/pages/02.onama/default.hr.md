---
title: O nama
slug: o-nama
---

Infraterra d.o.o. osnovana je 1996. g. kao tvrtka u privatnom vlasništvu i registrirana u Trgovačkom sudu u Zagrebu, za obavljanje djelatnosti projektiranja, nadzora, konzaltinga i građenja.

Društvo smo s tradicijom neprekidnog rada preko 20 godina i velikim intelektualnim kapitalom, spremnim za najkompleksnije poslove. Stručnjaci tvrtke su ovlašteni inženjeri s iskustvom na izradi projektne dokumentacije, građenju i obavljanju stručnog nadzora.

Znanjem i iskustvom pokrivamo sve faze projekata širokog spektara infrastrukturnih građevina kao što su magistralni plinovodi, sustavi vodoopskrbe, odvodnje, regulacije vodotoka i uređaji za pročišćavanje otpadnih voda.

U radu primjenjujemo suvremene tehnologije i timski rad, koji uključuje klijente, što smatramo preduvjetom uspješne realizacije svih projekata. Osobitu pažnju posvećujemo usavršavanju mladog kadra, koje provodimo neprestanim učenjem i praćenjem dostignuća i spoznaja.

Ponosni smo na veliki broj naših realiziranih projekata, kao i one koji se upravo realiziraju, a posebno na iskustvo u izradi studijske dokumentacije i prijavu projekata za sufinanciranje iz fondova EU.

#####Pružamo sljedeće usluge:
* Stručni nadzor građenja
* Vođenja projekata (IPMA 4-Level-C)
* Koordinatora 1 i 2 zaštite na radu
* Studije izvedivosti za hidrotehničke sustave (vodoopskrbe, odvodnje, pročišćavanje otpadnih voda)
* Koncepcijska rješenja
* Idejni, glavni i izvedbeni projekti
* Vođenje gradnje i građenje linijskih objekata i UPOVA
