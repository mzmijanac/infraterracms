---
title: Infraterra project
slug: infraterra-project
date: 04/07/2017
pub: Published on
taxonomy:
    category: blog
markdown:
    extra: true
---
On December 8, 2016, Infraterra d.o.o. started with the implementation of the project on Improving competitiveness and innovation through information and communication technologies (ICT) under the name Project Infraterra. The project is co-financed by the European Regional Development Fund through the Operational Program Competitiveness and Cohesion 2014-2020 as a part of the tender concerning the Improvement of competitiveness and efficiency of SMEs in areas with developmental specificities through information and communication technologies (ICT).

#####General project information:
**Project name:** Infraterra<br>
**Brief project description:** Improving Competitiveness and Efficiency through Information and Communication Technologies (ICT)<br>
**Objectives and expected project results:** Education in the use of new technologies and software, procuration of modern IT equipment for a faster and better processing of services we provide, all with the aim of increasing the work volume and new employment.<br>
**Total project value:** HRK 265.005,96<br>
**Amount co-financed by the EU:** HRK 235.828,80<br>
**Period for project implementation:** from 8 Dec 2016 to 8 Dec 2017<br>
**Contact person for additional information:** Emil Krznarić

<div class="blog-images">
    <img class="blog-img" src="./user/pages/01.home/projekt-infraterra/ESI.png" alt="Europski strukturni i investicijksi fondovi">
    <img class="blog-img" src="./user/pages/01.home/projekt-infraterra/OPKK.png" alt="Operativni program konkurentnost i kohezija">
</div>
