---
title: Projekt Infraterra
slug: projekt-infraterra
date: 04/07/2017
pub: Objavljeno
taxonomy:
    category: blog
markdown:
    extra: true
---
Na dan 08.12.2016. godine tvrtka Infraterra d.o.o. započela je s provedbom projekta Poboljšanja konkurentnosti i unčinkovitosti kroz informacijske i komunikacijske tehnologije (IKT) pod nazivom Projekt Infraterra. Projekt je sufinanciran sredstvima iz Europskog fonda za regionalni razvoj kroz Operativni program Konkurentnost i kohezija 2014.-2020. u sklopu natječaja Poboljšanje konkurentnosti i učinkovitosti MSP u područjima s razvojnim posebnostima kroz informacijske i komunikacijske tehnologije (IKT).

#####Osnovni podaci o projektu:
**Naziv projekta:** Infraterra<br>
**Kratki opis projekta:** Poboljšanje konkurentnosti i učinkovitosti kroz informacijske i komunikacijske tehnologije (IKT)<br>
**Ciljevi i očekivani rezultati projekta:** Edukacija u korištenju novih tehnologija, softwarea i opremanje modernom informatičkom opremom radi bržeg i kvalitetnijeg procesuiranja usluga koje pružamo, a sve s ciljem povećanja obujma posla i novih zapošljavanja.<br>
**Ukupnu vrijednost projekta:** 265.005,96 kuna<br>
**Iznos koji sufinancira EU:** 235.828,80 kuna<br>
**Razdoblje provedbe projekta:** od 08. prosinca 2016. do 08. prosinca 2017.<br>
**Kontakt osobe za više informacija:** Emil Krznarić

<div class="blog-images">
    <img class="blog-img" src="./user/pages/01.home/projekt-infraterra/ESI.png" alt="Europski strukturni i investicijksi fondovi">
    <img class="blog-img" src="./user/pages/01.home/projekt-infraterra/OPKK.png" alt="Operativni program konkurentnost i kohezija">
</div>
