---
title: News
slug: news
blog_url: blog

sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items: @self.children
    order:
        by: date
        dir: desc
    limit: 5
    pagination: false

feed:
    description: Sample Blog Description
    limit: 10

pagination: false

one: Design
onecontent: We offer complete design services and we can perform all the actions necessary for the conception, design and implementation of a specific project. Our functional, shaping and mutually matching technical solutions save energy, time and money for clients.

two: Supervision
twocontent: We offer expert supervision in all stages of the construction. As part of expert supervision services, we coordinate the work of all the construction participants, we take part in technical examinations, draw up final reports, advise investors, up until the phase of obtaining the usage inspection certificate.

three: Construction
threecontent: We provide construction management services, and based on the contract on business and technical cooperation, we can offer complete services for the construction of pipelines and wastewater treatment plants.

more: READ MORE
---
