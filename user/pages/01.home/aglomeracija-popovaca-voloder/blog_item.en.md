---
title: Agglomeration Popvača and Voloder
slug: aglomeration
date: 03/16/2016
pub: Published on
taxonomy:
    category: blog
markdown:
    extra: true
---
We signed a contract for drawing up study documentation and project application for construction of the communal infrastructure of Agglomeration Popovača and Voloder for co-financing from the EU funds.

**Investment total:** 2.580.050,00 HRK<br>
**Cohesion fund:** 2.193.042,50 HRK<br>
**Partners:** 387.007,50 HRK<br>

Development of study documentation for Agglomeration Popovača and Voloder is ongoing.

#####Follow the other stages of the project:
1. Feasibility study and application document
2. Environmental Impact Assessment
3. Investigative works
4. Elaborates for property legal relations
5. Preliminary design
6. Main design
7. Drawing up tender documentation
8. Advertising and visibility

<br>
**Client:** [Moslavina d.o.o.](http://www.moslavina-kutina.hr/)<br>
**Partner:** [Grad Popovača](http://www.popovaca.hr/)<br>
**Contractors:**
* [Infraterra d.o.o.](http://www.infraterra.com.hr)
* [Safege d.o.o.](http://www.safege.com/)
* [H5 d.o.o.](http://hpet.hr/)
* [Via Factum d.o.o.](http://www.viafactum.hr/)

**Links:**
* [European Union](http://europa.eu/european-union/index_en)
* [Operativni program zaštita okoliša](http://www.opzo.hr/)
* [Europski strukturni i investicijski fondovi](http://www.strukturnifondovi.hr/)
* [Ministarstvo poljoprivrede](http://www.mps.hr/)
* [Hrvatske vode](http://www.voda.hr/)

<div class="blog-images">
    <img class="blog-img2" src="./user/pages/01.home/aglomeracija-popovaca-voloder/letak-1.jpg" alt="Aglomeracija Popovača i Voloder Letak"></a>
    <img class="blog-img2" src="./user/pages/01.home/aglomeracija-popovaca-voloder/letak-2.jpg" alt="Aglomeracija Popovača i Voloder Letak"></a>
</div>
