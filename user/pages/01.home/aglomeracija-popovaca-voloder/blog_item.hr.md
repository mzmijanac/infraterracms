---
title: Aglomeracija Popovača i Voloder
slug: aglomeracija
date: 03/16/2016
pub: Objavljeno
taxonomy:
    category: blog
markdown:
    extra: true
---
Ugovorili smo izradu studijske i projektne dokumentacije za prijavu izgradnje vodnokomunalne infrastrukture Aglomeracije Popovača i Voloder za sufinanciranje iz fondova EU.

**Investicija ukupno:** 2.580.050,00 kuna</br>
**Kohezijski fond:** 2.193.042,50 kuna</br>
**Partneri:** 387.007,50 kn</br>

U tijeku je izrada studijske dokumentacije za Aglomeraciju Popovača i Voloder.

#####Pratite i ostale faze projekta:
1. Studija izvedivosti i prijavni dokument
2. Procjena utjecaja na okoliš
3. Aplikacijski paket
4. Istražni radovi
5. Geodetski projekt
6. Elaborat prava služnosti/nepotpunog izvlaštenja
7. Idejni projekti
8. Glavni projekti
9. Izrada dokumentacije za nadmetanje
10. Promidžba i vidljivost

<br>
**Naručitelj:** [Moslavina d.o.o.](http://www.moslavina-kutina.hr/)<br>
**Partner:** [Grad Popovača](http://www.popovaca.hr/)<br>
**Izvršitelji:**
* [Infraterra d.o.o.](http://www.infraterra.com.hr)
* [Safege d.o.o.](http://www.safege.com/)
* [H5 d.o.o.](http://hpet.hr/)
* [Via Factum d.o.o.](http://www.viafactum.hr/)

**Poveznice:**
* [European Union](http://europa.eu/european-union/index_en)
* [Operativni program zaštita okoliša](http://www.opzo.hr/)
* [Europski strukturni i investicijski fondovi](http://www.strukturnifondovi.hr/)
* [Ministarstvo poljoprivrede](http://www.mps.hr/)
* [Hrvatske vode](http://www.voda.hr/)

<div class="blog-images">
    <img class="blog-img2" src="./user/pages/01.home/aglomeracija-popovaca-voloder/letak-1.jpg" alt="Aglomeracija Popovača i Voloder Letak"></a>
    <img class="blog-img2" src="./user/pages/01.home/aglomeracija-popovaca-voloder/letak-2.jpg" alt="Aglomeracija Popovača i Voloder Letak"></a>
</div>

