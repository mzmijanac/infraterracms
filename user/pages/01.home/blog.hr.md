---
title: Novosti
slug: novosti
blog_url: blog

sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items: @self.children
    order:
        by: date
        dir: desc
    limit: 5
    pagination: false

feed:
    description: Sample Blog Description
    limit: 10

pagination: false

one: Projektiranje
onecontent: Nudimo cjelovite usluge projektiranja te možemo obaviti sve radnje potrebne za promišljanje, izradu i provođenje određenog projekta. Naša oblikovno-funkcionalna i međusobno usklađena tehnička rješenja klijentima štede energiju, vrijeme i novac.

two: Stručni nadzor
twocontent: Vršimo stručni nadzor nad izgradnjom građevinskih objekata u svim fazama izgradnje. U sklopu pružanja usluga stručnog nadzora, koordiniramo rad svih sudionika u gradnji, sudjelujemo na tehničkim pregledima, izrađujemo završna izvješća, savjetujemo investitora, sve do završne faze ishođenja uporabne dozvole.

three: Građenje
threecontent: Pružamo uslugu vođenja gradnje, a temeljem ugovora o poslovno-tehničkoj suradnji sa našim suradnicima možemo ponuditi cjelovite usluge građenja linijskih objekata i uređaja za pročišćavanje otpadnih voda.

more: Saznaj više
---
