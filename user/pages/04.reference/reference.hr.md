---
title: Reference
slug: reference
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>Projektiranje</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Stručni nadzor</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Građenje</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="reference-list">
                <li><a href="./reference/projektiranje-vodoopskrba/"><img src="../user/pages/04.reference/vodoopskrba.png" alt="Vodoopskrba">Vodoopskrba</a></li>
                <li><a href="./reference/projektiranje-odvodnja/"><img src="../user/pages/04.reference/odvodnja.png" alt="Odvodnja">Odvodnja</a></li>
                <li><a href="./reference/projektiranje-otpadne-vode/"><img src="../user/pages/04.reference/otpadne-vode.png" alt="Pročišćavanje otpadnih voda">Pročišćavanje otpadnih voda</a></li>
                <li><a href="./reference/projektiranje-plinovodi/"><img src="../user/pages/04.reference/plinovodi.png" alt="Plinovodi">Plinovodi</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="reference-list">
                <li><a href="./reference/nadzor-plinovodi/"><img src="../user/pages/04.reference/magistralni-plinovodi.png" alt="Magistralni plinovodi">Magistralni plinovodi</a></li>
                <li><a href="./reference/nadzor-otpadne-vode/"><img src="../user/pages/04.reference/prociscavanje-otpadnih-voda.png" alt="Vodocrpilišta i uređaji za pročišćavaje otpadnih voda">Vodocrpilišta i uređaji za pročišćavaje otpadnih voda</a></li>
                <li><a href="./reference/nadzor-vodoopskrba/"><img src="../user/pages/04.reference/vodotok.png" alt="Vodoopskrba, odvodnja, regulacije vodotoka i ceste">Vodoopskrba, odvodnja, regulacije vodotoka i ceste</a></li>
                <li><a href="./reference/nadzor-trgcentri/"><img src="../user/pages/04.reference/shopping.png" alt="Trgovački centri i benzinske postaje">Trgovački centri</a></li>
                <li><a href="./reference/nadzor-benzpostaje/"><img src="../user/pages/04.reference/benzinska.png" alt="Trgovački centri i benzinske postaje">Benzinske postaje</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="reference-list">
                <li><img src="../user/pages/04.reference/gradjenje.svg">Uskoro</li>
            </ul>
        </div>
    </div>
</div>

