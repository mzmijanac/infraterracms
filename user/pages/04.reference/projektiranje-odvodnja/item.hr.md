---
titlePath: Projektiranje
title: Odvodnja
slug: projektiranje-odvodnja
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>Reference</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Klijenti</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Djelatnici</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Aglomeracija Kutina – Faza 8 - Izrada idejnih i glavnih projekata odvodnje</a></li>
                <li><a href="">Aglomeracija Kutina – Faza 7 - Izrada idejnih i glavnih projekata odvodnje</a></li>
                <li><a href="">Aglomeracija Kutina – Faza 1 i 2 - Izrada idejnih i glavnih projekata odvodnje</a></li>
                <li><a href="">Idejni i glavni projekt odvodnje kolektora K47 u Industrijsko-logističkoj zoni grada Kutine </a></li>
                <li><a href="">Aglomeracija Sinj – Izrada idejnih i glavnih projekata odvodnje</a></li>
                <li><a href="">Aglomeracija Zabok – Općina Stubičke toplice- Izrada idejnih i glavnih projekata</a></li>
                <li><a href="">Aglomeracija Novska – Naselje Bročice-Izrada idejnih i glavnih projekata</a></li>
                <li><a href="">Aglomeracija Mursko Središće -naselja Peklenica i Vratišinec</a></li>
                <li><a href="">Koncepcijsko rješenje, idejni i glavni projekt kanalizacije u Kloštar lvaniću</a></li>
                <li><a href="">Idejni, glavni i izvedbeni projekt sustav odvodnje grada Poreča i prigradskih naselja</a></li>
                <li><a href="">Koncepcijsko rješenje, Idejni i glavni projekt sustava odvodnje naselja Bednja s 2 uređaja za pročišćavanje</a></li>
                <li><a href="">Idejni i glavni projekti sustava odvodnje naselja Marijanci, Kunišinci i Črnkovci</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://www.moslavina-kutina.hr">Moslavina d.o.o.</a></li>
                <li><a href="https://vodovod-novska.hr">Vodovod Novska d.o.o.</a></li>
                <li><a href="http://www.viock.hr/">Vodovod i odvodnja Cetinske Krajine d.o.o.</a></li>
                <li><a href="http://www.ivkom-vode.hr/">Ivkom-Vode d.o.o.</a></li>
                <li><a href="http://www.klostar-ivanic.hr/">Općina Kloštar Ivanić</a></li>
                <li><a href="http://www.usluga.hr/">Usluga Poreč</a></li>
                <li><a href="https://www.hidrobel.hr/">Hidrobel d.o.o.</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/ivana-varga-065155b2">Ivana Varga, mag.ing.aedif.</a></li>
                <li><a href="#">Zdravko Drmić, dipl.ing.geol.</a></li>
                <li><a href="http://linkedin.com/in/matija-dunatov-02868710b">Matija Dunatov, mag.ing.aedif.</a></li>
                <li><a href="#">Leon Krznarić</a></li>
            </ul>
        </div>
    </div>
</div>
