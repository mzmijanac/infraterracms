---
titlePath: Design
title: Drainage
slug: design-drainage
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>References</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Clients</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Employees</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Agglomeration Kutina -  Phase 8 - preliminary and main drainage designs</a></li>
                <li><a href="">Agglomeration Kutina – Phase 7 - preliminary and main drainage designs</a></li>
                <li><a href="">Agglomeration Kutina – Phases 1 and 2 - preliminary and main drainage designs</a></li>
                <li><a href="">Preliminary and main drainage design of the collector K47 in the industrial and logistic zone Kutina</a></li>
                <li><a href="">Agglomeration Sinj – preliminary and main drainage designs</a></li>
                <li><a href="">Agglomeration Zabok – Municipality Stubičke toplice- preliminary and main drainage designs</a></li>
                <li><a href="">Agglomeration Novska –Bročice community - preliminary and main drainage designs</a></li>
                <li><a href="">Agglomeration Mursko Središće - Peklenica and Vratišinec communities</a></li>
                <li><a href="">Conceptual solution, preliminary and main sewer design in Kloštar Ivanić</a></li>
                <li><a href="">Preliminary, main and construction design of the drainage system in Poreč and suburbs</a></li>
                <li><a href="">Conceptual solution, preliminary and main drainage design for the community Bednja with 2 purification devices</a></li>
                <li><a href="">Preliminary and main drainage design for the communities Marijanci, Kunišinci i Črnkovci</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://www.moslavina-kutina.hr">Moslavina d.o.o.</a></li>
                <li><a href="https://vodovod-novska.hr">Vodovod Novska d.o.o.</a></li>
                <li><a href="http://www.viock.hr/">Vodovod i odvodnja Cetinske Krajine d.o.o.</a></li>
                <li><a href="http://www.ivkom-vode.hr/">Ivkom-Vode d.o.o.</a></li>
                <li><a href="http://www.klostar-ivanic.hr/">Općina Kloštar Ivanić</a></li>
                <li><a href="http://www.usluga.hr/">Usluga Poreč</a></li>
                <li><a href="https://www.hidrobel.hr/">Hidrobel d.o.o.</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/ivana-varga-065155b2">Ivana Varga, mag.ing.aedif.</a></li>
                <li><a href="#">Zdravko Drmić, dipl.ing.geol.</a></li>
                <li><a href="http://linkedin.com/in/matija-dunatov-02868710b">Matija Dunatov, mag.ing.aedif.</a></li>
                <li><a href="#">Leon Krznarić</a></li>
            </ul>
        </div>
    </div>
</div>
