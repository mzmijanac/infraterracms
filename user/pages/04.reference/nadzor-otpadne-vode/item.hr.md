---
titlePath: Stručni nadzor
title: Vodocrpilišta i uređaji za pročišćavanje otpadnih voda
slug: nadzor-otpadne-vode
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>Reference</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Klijenti</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Djelatnici</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Konzultantkse usluge stručnog nadzora nad projektiranjem, nabavom i izgradnjom uređaja za pročišćavanje otpadnih voda grada Dugog Sela, 25.000ES (žuti FIDIC)</a></li>
                <li><a href="">Kompletan stručni nadzor nad projektiranjem, izgradnjom i testiranjem vodocrpilišta Topolje 40l/s i Prosine 30 l/s i vodoopskrbe sjeverne Baranje, pram pravilima žutog fidica i koordinatora zaštite na radu</a></li>
                <li><a href="">Stručni nadzor nad rekonstrukcijom vodocrpilišta i postrojenja za pripremu pitke vode u gradu Popovača, kapaciteta 80l/s</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://dukom.hr"></a>Dukom d.o.o.</li>
                <li><a href="http://www.baranjski-vodovod.hr"></a>Baranjski vodovod d.o.o.</li>
                <li><a href="http://www.moslavina-kutina.hr"></a>Moslavina d.o.o.</li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/tajana-krznaric-788270143">Tajana Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
            </ul>
        </div>
    </div>
</div>
