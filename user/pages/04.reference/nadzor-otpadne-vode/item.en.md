---
titlePath: Supervision
title: Wellfields and wastewater...
slug: supervision-wastewater
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>References</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Clients</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Employees</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Consulting services for expert supervision on the design, procurement and construction of wastewater treatment plants in Dugo Selo, 25.000ES (yellow FIDIC)</a></li>
                <li><a href="">Complete expert supervision on the design, construction and testing of the Topolje 40 l/s and Prosine 30 l/s wellfields and of the water supply of North Baranja, in accordance to yellow FIDIC and occupational safety coordinator service</a></li>
                <li><a href="">Expert supervision on reconstruction of the wellfields and plants for the preparation of drinking water in Popovača, capacity 80 l/s</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://dukom.hr"></a>Dukom d.o.o.</li>
                <li><a href="http://www.baranjski-vodovod.hr"></a>Baranjski vodovod d.o.o.</li>
                <li><a href="http://www.moslavina-kutina.hr"></a>Moslavina d.o.o.</li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/tajana-krznaric-788270143">Tajana Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
            </ul>
        </div>
    </div>
</div>
