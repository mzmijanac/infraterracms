---
titlePath: Projektiranje
title: Vodoopskrba
slug: projektiranje-vodoopskrba
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>Reference</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Klijenti</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Djelatnici</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Izrada glavnog i izvedbenog projekta magistralnog i opskrbnog sustava vodovodne mreže na području općine Jakovlje – opskrba vodom naselja Kraljev Vrh</a></li>
                <li><a href="">Izrada idejnog, glavnog i izvedbenog projekta vodoopskrbnog cjevovoda od Zajčeve do Srebrenjaka i prespoj u Petrovoj ulici u Zagrebu</a></li>
                <li><a href="">Izrada idejnog, glavnog i izvedbenog projekta vodoopskrbnog cjevovoda u ulici J.E. Tomića, Čret i Jazbinskim odvojcima, Veliki Dol i Županijskoj u Zagrebu</a></li>
                <li><a href="">Izrada idejnog i glavnog projekta spojnog vodoopskrbnog cjevovoda Dolina-Mačkovac-Savski Bok-Visoka Greda i opskrbni cjevovod</a></li>
                <li><a href="">Izrada idejnog i glavnog projekta vodoopskrbne mreže naselja Ljupina</a></li>
                <li><a href="">Izrada glavnog i izvedbenog projekta distributivnog vodovoda naselja Markovac</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://www.vio.hr">Vodoopskrba i odvodnja d.o.o. Zagreb</a></li>
                <li><a href="https://www.zagorski-vodovod.hr">Zagorski vodovod d.o.o.</a></li>
                <li><a href="">Regionalni vodovod Davor – Nova Gradiška d.o.o.</a></li>
                <li><a href="http://www.nova-gradiska.hr">Grad Nova Gradiška</a></li>
                <li><a href="http://www.darkom-daruvar.hr">Darkom d.o.o.</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/ivana-varga-065155b2">Ivana Varga, mag.ing.aedif.</a></li>
                <li><a href="#">Zdravko Drmić, dipl.ing.geol.</a></li>
                <li><a href="http://linkedin.com/in/matija-dunatov-02868710b">Matija Dunatov, mag.ing.aedif.</a></li>
                <li><a href="#">Leon Krznarić</a></li>
            </ul>
        </div>
    </div>
</div>
