---
titlePath: Design
title: Water supply
slug: design-water-supply
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>References</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Clients</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Employees</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Main and construction design of the watermain and water supply system in the municipal area of Jakovlje - water supply of the community Kraljev Vrh</a></li>
                <li><a href="">Preliminary, main and construction design of the water supply pipeline from Zaječva to Srebrenjak and the relinking in Petrova street in Zagreb</a></li>
                <li><a href="">Preliminary, main and construction design of the water supply pipeline in the street J.E. Tomić, Čret and Jazbinski branches, Veliki Dol and Županijska in Zagreb</a></li>
                <li><a href="">Preliminary, main and construction design of the water supply pipeline Dolina-Mačkovac-Savski Bok-Visoka Greda and supply pipeline</a></li>
                <li><a href="">Preliminary and main design of the water supply network of the Ljupina community</a></li>
                <li><a href="">Main and construction design of the water distribution pipeline of the Markovac community</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://www.vio.hr">Vodoopskrba i odvodnja d.o.o. Zagreb</a></li>
                <li><a href="https://www.zagorski-vodovod.hr">Zagorski vodovod d.o.o.</a></li>
                <li><a href="">Regionalni vodovod Davor – Nova Gradiška d.o.o.</a></li>
                <li><a href="http://www.nova-gradiska.hr">Grad Nova Gradiška</a></li>
                <li><a href="http://www.darkom-daruvar.hr">Darkom d.o.o.</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/ivana-varga-065155b2">Ivana Varga, mag.ing.aedif.</a></li>
                <li><a href="#">Zdravko Drmić, dipl.ing.geol.</a></li>
                <li><a href="http://linkedin.com/in/matija-dunatov-02868710b">Matija Dunatov, mag.ing.aedif.</a></li>
                <li><a href="#">Leon Krznarić</a></li>
            </ul>
        </div>
    </div>
</div>
