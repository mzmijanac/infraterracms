---
titlePath: Supervision
title:
slug: supervision-gas-pipelines
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>References</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Clients</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Employees</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Complete expert supervision on the construction of the Main gas pipeline Vodnjan – Umag DN/BAR 300/50, 73km</a></li>
                <li><a href="">Complete expert supervision on the construction of the Measuring reduction station Rijeka Istok DN/BAR 500/100/75</a></li>
                <li><a href="">Complete expert supervision on the construction of the Branch pipeline for MRS Otočac DN/BAR 200/75, 10,5km</a></li>
                <li><a href="">Complete expert supervision on the construction of the Branch pipeline for MRS Gospić DN/BAR 200/75, 700m</a></li>
                <li><a href="">Complete expert supervision on the construction of the Measuring reduction station Otočac DN/BAR 200/75/16/4</a></li>
                <li><a href="">Complete expert supervision on the construction of the Measuring reduction station Gospić DN/BAR 200/75/16/4</a></li>
                <li><a href="">Complete expert supervision on the construction of the Main gas pipeline Josipdol-Gospić DN/BAR 500/75, 83,5km</a></li>
                <li><a href="">Complete expert supervision on the construction of the Main gas pipeline Slobodnica-Donji Miholjac DN/BAR 800/75, 73km</a></li>
                <li><a href="">Complete expert supervision on the construction of the Main gas pipeline Donji Miholjac-Drava DN/BAR 800/75, 7,6km</a></li>
                <li><a href="">Complete expert supervision on the construction of the Main gas pipeline Gospić-Benkovac DN/BAR 500/75, 91km</a></li>
                <li><a href="">Complete expert supervision on the construction of the Measuring reduction stations Biograd, Obrovac, Zadar, Knin, Drniš, Trogir, Tisno, Gračac, Benkovac, Šibenik and Split with drainage pipeline, total length 125km</a></li>
                <li><a href="">Complete expert supervision on the construction of the Main gas pipeline Benkovac-Dugopolje, DN/BAR 500/75, 96km</a></li>
                <li><a href="">Complete expert supervision on the reconstruction of the gas junction Lučko</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://www.plinacro.hr/">Plinacro d.o.o.</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/tajana-krznaric-788270143">Tajana Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
            </ul>
        </div>
    </div>
</div>
