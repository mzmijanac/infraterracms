---
titlePath: Design
title: Wastewater treatment
slug: design-wastewater-treatment
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>References</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Clients</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Employees</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Preliminary design of the wetland plant for wastewater treatment with a capacity of 1,900 ES</a></li>
                <li><a href="">Preliminary and main design for drainage system and wastewater treatment plants 900 ES in Bednja community</a></li>
                <li><a href="">Preliminary and main design for drainage system in Kloštar Ivanić with 2 biological wastewater treatment plants 170 ES</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://www.ivkom-vode.hr/">Ivkom-Vode d.o.o.</a></li>
                <li><a href="http://www.klostar-ivanic.hr/">Općina Kloštar Ivanić</a></li>
                <li><a href="">Martinela d.o.o.</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/ivana-varga-065155b2">Ivana Varga, mag.ing.aedif.</a></li>
                <li><a href="#">Zdravko Drmić, dipl.ing.geol.</a></li>
                <li><a href="http://linkedin.com/in/matija-dunatov-02868710b">Matija Dunatov, mag.ing.aedif.</a></li>
                <li><a href="#">Leon Krznarić</a></li>
            </ul>
        </div>
    </div>
</div>
