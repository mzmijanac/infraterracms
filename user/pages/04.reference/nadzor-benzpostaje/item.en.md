---
titlePath: Supervision
title:  Gas stations
slug: supervision-gasstations
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>References</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Clients</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Employees</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Expert supervision of the construction of the gas station in Opatija</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://www.adriaoil.hr">Adria Oil d.o.o.</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/tajana-krznaric-788270143">Tajana Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
            </ul>
        </div>
    </div>
</div>
