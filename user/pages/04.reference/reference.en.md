---
title: References
slug: references
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>Design</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Supervision</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Construction</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="reference-list">
                <li><a href="./references/design-water-supply/"><img src="../user/pages/04.reference/vodoopskrba.png" alt="Water supply">Water supply</a></li>
                <li><a href="./references/design-drainage/"><img src="../user/pages/04.reference/odvodnja.png" alt="Drainage">Drainage</a></li>
                <li><a href="./references/design-wastewater-treatment/"><img src="../user/pages/04.reference/otpadne-vode.png" alt="Wastewater treatment">Wastewater treatment</a></li>
                <li><a href="./references/design-gas-pipelines/"><img src="../user/pages/04.reference/plinovodi.png" alt="Gas pipelines">Gas pipelines</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="reference-list">
                <li><a href="./references/supervision-gas-pipelines/"><img src="../user/pages/04.reference/magistralni-plinovodi.png" alt="Main gas pipelines">Main gas pipelines</a></li>
                <li><a href="./references/supervision-wastewater/"><img src="../user/pages/04.reference/prociscavanje-otpadnih-voda.png" alt="Wellfields and wastewater treatment plants">Wellfields and wastewater treatment plants</a></li>
                <li><a href="./references/supervision-water-supply/"><img src="../user/pages/04.reference/vodotok.png" alt="Water supply, drainage, watercourse and road regulation">Water supply, drainage, watercourse and road regulation</a></li>
                <li><a href="./references/supervision-commercial/"><img src="../user/pages/04.reference/shopping.png" alt="Commercial areas">Shopping mall</a></li>
                <li><a href="./references/supervision-gasstations/"><img src="../user/pages/04.reference/benzinska.png" alt="Gas stations">Gas stations</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="reference-list">
                <li><img src="../user/pages/04.reference/gradjenje.svg">Soon...</li>
            </ul>
        </div>
    </div>
</div>

