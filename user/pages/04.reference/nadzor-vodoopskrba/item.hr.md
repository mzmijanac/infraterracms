---
titlePath: Stručni nadzor
title: Vodoopskrba, odvodnja, regulacije vodotoka i ceste
slug: nadzor-vodoopskrba
markdown:
    extra: true
---
<div class="references-container">
    <div class="references">
        <ul class="references-tab-links">
            <li id="projektiranje" class="references-tab-link active" data-tab="tab-1"><h4>Reference</h4></li>
            <span class="references-tab-divider"></span>
            <li id="nadzor" class="references-tab-link" data-tab="tab-2"><h4>Klijenti</h4></li>
            <span class="references-tab-divider"></span>
            <li id="građenje" class="references-tab-link" data-tab="tab-3"><h4>Djelatnici</h4></li>
        </ul>
        <div id="tab-1" class="references-tab active">
            <ul class="item-list">
                <li><a href="">Stručni nadzor nad rekonstrukcijom ceste, izgradnjom nogostupa i oborinske kanalizacije u Vrbovcu</a></li>
                <li><a href="">Stručni nadzor nad izgradnjom prometnica, vodovoda, oborinske i fekalne kanalizacije i visokonaponske mreže za poduzetničku zonu Velika Ludina</a></li>
                <li><a href="">Stručni nadzor nad radovima izgradnje magistralnog odvodnog cjevovoda sustava odvodnje naselja Pušća i gravitirajućih naselja</a></li>
                <li><a href="">Konzultantske usluge kompletnog stručnog nazdora nad izgradnjom nasipa Trebarjevo desno</a></li>
            </ul>
        </div>
        <div id="tab-2" class="references-tab">
            <ul class="item-list">
                <li><a href="http://www.vrbovec.hr/">Grad Vrbovec</a></li>
                <li><a href="http://www.moslavina-kutina.hr/">Moslavina d.o.o.</a></li>
                <li><a href="http://www.komunalno-zapresic.hr/">Zaprešić d.o.o.</a></li>
            </ul>
        </div>
        <div id="tab-3" class="references-tab">
            <ul class="item-list">
                <li><a href="http://linkedin.com/in/tajana-krznaric-788270143">Tajana Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/tomislav-krznaric-232320143">Tomislav Krznarić, mag.ing.aedif.</a></li>
                <li><a href="http://linkedin.com/in/emil-krznaric-83928125">Emil Krznarić, mag.ing.aedif.</a></li>
            </ul>
        </div>
    </div>
</div>
