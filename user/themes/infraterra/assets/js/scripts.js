// Slider
$('.slider').flickity({
  // options
  cellAlign: 'center',
  setGallerySize: false,
  cellSelector: '.slider-item',
  autoPlay: 10000,
  pauseAutoPlayOnHover: false,
  contain: true
});

// Structure tabs
$('ul.structures-tab-links > li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.structures-tab-links li').removeClass('active');
    $('.structures-tab').removeClass('active');

    $(this).addClass('active');
    $("#"+tab_id).addClass('active');
});

// References tabs
$('ul.references-tab-links > li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.references-tab-links li').removeClass('active');
    $('.references-tab').removeClass('active');

    $(this).addClass('active');
    $("#"+tab_id).addClass('active');
});

// Contacts Tabs
$('ul.contacts-tab-links li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.contacts-tab-links li').removeClass('active');
    $('.contacts-tab').removeClass('active');

    $(this).addClass('active');
    $("#"+tab_id).addClass('active');
});

//Google map
var marker,
    map,
    title,
    currentLang,
    infowindow = new google.maps.InfoWindow();

function getCurrentLanguage(){
    return $('.infobar-box.lan.active > a > h4').html();
}

//Zagreb
$('#zg').click(function(){
    var lang = getCurrentLanguage();
    if ( lang == 'HR' ) {
        title = 'Ured';
    } else {
        title = 'Office';
    }
    updateMarker(45.798311, 15.9621536, title);

});

//Kutina
$('#kt').click(function(){
    var lang = getCurrentLanguage();
    if ( lang == 'HR' ) {
        title = 'Sjedište';
    } else {
        title = 'Headquarters';
    }
    updateMarker(45.4809981, 16.8091282, title);
});

function initMap() {
        var style = [{
            saturation: -100
        }];
        var styledMap = new google.maps.StyledMapType(style, {
            name: 'Styled Map'
        });

        var mapProperties = {
            center: new google.maps.LatLng(45.798311, 15.9621536),
            zoom: 13,
            panControl: false,

            mapTypeControl: false,
            scaleControl: true,
            streetViewControl: false,
            overviewMapControl: false,
            rotateControl: true,
            mapTypeID: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById('map'), mapProperties);

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        var icon = {
            path: 'M322.621,42.825C294.073,14.272,259.619,0,219.268,0c-40.353,0-74.803,14.275-103.353,42.825 c-28.549,28.549-42.825,63-42.825,103.353c0,20.749,3.14,37.782,9.419,51.106l104.21,220.986  c2.856,6.276,7.283,11.225,13.278,14.838c5.996,3.617,12.419,5.428,19.273,5.428c6.852,0,13.278-1.811,19.273-5.428 c5.996-3.613,10.513-8.562,13.559-14.838l103.918-220.986c6.282-13.324,9.424-30.358,9.424-51.106 C365.449,105.825,351.176,71.378,322.621,42.825z M270.942,197.855c-14.273,14.272-31.497,21.411-51.674,21.411 s-37.401-7.139-51.678-21.411c-14.275-14.277-21.414-31.501-21.414-51.678c0-20.175,7.139-37.402,21.414-51.675 c14.277-14.275,31.504-21.414,51.678-21.414c20.177,0,37.401,7.139,51.674,21.414c14.274,14.272,21.413,31.5,21.413,51.675 C292.355,166.352,285.217,183.575,270.942,197.855z',
            fillColor: '#195D7E',
            fillOpacity: 1,
            strokeWeight: 0,
            scale: 0.125,
            anchor: new google.maps.Point(200,450),
        }

        currentLang = getCurrentLanguage();
        if (currentLang == 'HR') {
            title = 'Ured';
        } else {
            title = 'Office';
        }

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(45.798311, 15.9621536),
            title: title,
            icon: icon,
            animation: google.maps.Animation.DROP
        });

        marker.setMap(map);
        map.panTo(marker.position);

        var html = '<div class="infowindow">' +
                '<h3> ' + marker.getTitle() + '</h3>' +
                '</div>';
        infowindow.setContent(html);

        setTimeout(function(){
            infowindow.open(map, marker);
        }, 1500);

        marker.addListener('click', function(){
            infowindow.open(map, marker);
        });
}

function updateMarker(lat, lon, title){
    myLatLng = new google.maps.LatLng(lat, lon);
    marker.setPosition(myLatLng);
    marker.setTitle(title);
    var html = '<div class="infowindow">' +
            '<h3> ' + title + '</h3>' +
            '</div>';
    infowindow.setContent(html);
    map.panTo(myLatLng);
}



google.maps.event.addDomListener(window, 'load', initMap);
