'use strict';

// ----------------------------------------------------------------------------
// Paths
// ----------------------------------------------------------------------------
var config = {
    projectName: 'Infraterra',
    browserSync: true,
    basePath: {
        development: './user/themes/infraterra/assets/src/',
        production: './user/themes/infraterra/assets/',
        twig: './user/themes/infraterra/',
        url: 'localhost'
    }
}

// ----------------------------------------------------------------------------
// Packages
// ----------------------------------------------------------------------------
// General
var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');

// SCSS
var sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer');

// JS
var uglify = require('gulp-uglify'),
    concat = require('gulp-concat');

// BrowserSync
var browserSync = require('browser-sync').create(config.projectName);


// ----------------------------------------------------------------------------
// Tasks
// ----------------------------------------------------------------------------
// SCSS Compiler, Autoprefixer
gulp.task('styles', () => {
    return gulp.src(config.basePath.development + 'scss/{,*/}/*.scss')
        .pipe(plumber())
        .pipe(sass({ includePaths: ['/scss/modules/', '/scss/partials/'] }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 10', 'and_chr >= 2.3'],
            cascade: false
        }))
        .pipe(gulp.dest(config.basePath.production + 'css/'))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

// Javascript Uglify & Concatenation
gulp.task('scripts', () => {
    return gulp.src([
            config.basePath.development + 'js/*.js'
        ])
        .pipe(plumber())
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(config.basePath.production + 'js/'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest(config.basePath.production + 'js/'))
        .pipe(browserSync.stream({match: '**/*.js'}));
});

// Default Build
gulp.task('default', () => {
    gulp.start('styles', 'scripts');
});

// Watch
gulp.task('watch', () => {
    if (config.browserSync == true) {
        browserSync.init({
            injectChanges: true,
            proxy: config.basePath.url
        });
    } else {
        console.log('BrowserSync disabled!');
    }

    // Watch .scss files
    gulp.watch(config.basePath.development + 'scss/{,*/}*.scss', ['styles']);

    // Watch .js files
    gulp.watch(config.basePath.development + 'js/*.js', ['scripts']);

    // Watch .twig files
    gulp.watch(config.basePath.twig + '/**/*.twig').on('change', browserSync.reload);
});

